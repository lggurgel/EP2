/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import model.Order;
import model.Product;

/**
 *
 * @author cienth
 */


        
public class OrderController {
    private ArrayList<Order> orderList;
    private final ClientController clientControlled;
    
    public OrderController(){
        clientControlled = new ClientController();
        this.loadOrders();
        
    }
    
    private void loadOrders(){
        ProductController productControlled = new ProductController();
        
        orderList = new ArrayList<>();
        
        try{
   
            InputStream is = new FileInputStream("db/orders/13-11-16-01order.txt");
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);

            String clientName = br.readLine();
            String total = br.readLine();         
            String observation = br.readLine();
            String product = br.readLine(); 
            ArrayList<Product> productList;
            
            

            while (clientName != null && total != null) {
                
                productList = new  ArrayList<>();
                while(!"end".equals(product)){
                productList.add(productControlled.searchProduct(product));
                
                product = br.readLine();
                }

                Order order = new Order(clientControlled.searchClient(clientName), 
                                        productList, observation, total);
                orderList.add(order);

                clientName = br.readLine();
                total = br.readLine();
                observation = br.readLine();
                product = br.readLine();
             
                

            }

            br.close();
        }catch(IOException e){
            
        }
        
    }
    
    public void saveOrders(){try{
   
            OutputStream os = new FileOutputStream("db/orders/13-11-16-01order.txt");
            OutputStreamWriter osw = new OutputStreamWriter(os);
            BufferedWriter bw = new BufferedWriter(osw);

            for (Order o : orderList){
                
                bw.write(o.getClient().getName());
                bw.newLine();
                bw.write(o.getTotal());
                bw.newLine();
                bw.write(o.getClientObservation());
                bw.newLine();
                
                for (Product p : o.getProductList()){
                    bw.write(p.getName());
                    bw.newLine();
                }
                bw.write("end");
                bw.newLine();
                
                
            }
            

            bw.close();
            
        }catch(IOException e){
            
        }
        
        
    }

    public ArrayList<Order> getOrderList() {
        return orderList;
    }
    
    public void addToOrderList(Order order){
        this.orderList.add(order);
        this.saveOrders();
        this.loadOrders();
    }
    
   public Order searchOrder(String name){
        
        for (Order o : orderList){
            if (name.equals(o.getClient().getName())) return o;
        }
        
        return null;
        
    }

    public void removeFromPedingOrder(Order order) {
        orderList.remove(order);
        this.saveOrders();
        this.loadOrders();
        
    }
    
    
    
}
