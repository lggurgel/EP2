/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;


/**
 *
 * @author cienth
 */
public abstract class Product {
 
    private String name;
    private String price;
    private Integer ammount;
    private Integer minAmmount;
    
    private final int id = 0;
    
    
    public Product(String name, String price, Integer ammount){
        
        this.name = name;
        this.price = price;
        this.ammount = ammount;
        
        
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public Integer getAmmount() {
        return ammount;
    }

    public void setAmoount(Integer ammount) {
        this.ammount = ammount;
    }

    public Integer getMinAmmount() {
        return minAmmount;
    }

    public void setMinAmmount(Integer minAmmount) {
        this.minAmmount = minAmmount;
    }

    public int getId() {
        return id;
    }
       
    
    
    
}
