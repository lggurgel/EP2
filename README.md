# EP2 - OO (UnB - Gama)

Este projeto consiste em uma aplicação desktop para um restaurante, utilizando a técnologia Java Swing.

## Executar projeto

O projeto ainda não está em sua versão compactada, então será necessário, de preferência, o NetBeans IDE.

Após baixado, abra um novo projeto no netBeans e selecione a pasta do EP2.

### Algumas considerações durante execução

1. Primeiramente é necessário entrar com credenciais. No arquivo db/clients.txt é possível modificar e acessar essas credenciais. 
2. Selecione o cliente
3. Selecione o produto à ser inserido no pedido do cliente
4. Após verificar o resumo do pedido, Make Order irá colocar o pedido realizado em modo de pendencia na aba (Pending Orders) até que o cliente faça o pagamento.
5. É possível adicionar novos clientes, novos produtos e também removê-los usando o modo admnistrador que encontra-se ao canto inferior direito do programa.
