/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author cienth
 */

public class Dessert extends Product{
    
    private final int id = 3;
    
    public Dessert(String name, String price, Integer ammount) {
        
        super(name, price, ammount);
    }
    
    
    public int getId() {
        return id;
    }
    
    
}
