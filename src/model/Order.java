/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;

/**
 *
 * @author cienth
 */
public class Order {
    Client client;
    ArrayList<Product> productList;
    String clientObservation;
    String total;
    
    
    public Order(Client client, ArrayList<Product> productList, String observation, String total){
        
        this.client = client;
        this.productList = productList;
        this.clientObservation = observation;
        this.total = total;
        
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public ArrayList<Product> getProductList() {
        return productList;
    }

    public void setProductList(ArrayList<Product> productList) {
        this.productList = productList;
    }

    public String getClientObservation() {
        return clientObservation;
    }

    public void setClientObservation(String clientObservation) {
        this.clientObservation = clientObservation;
    }

    public String getTotal() {
        return total;
    }
    
    
}
